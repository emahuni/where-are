# where-are
[semantic-release]: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg

> Find the nearest path of the requested target files/dirs starting from given directory or cwd. Also satisfy predicate if given.

## Install

``` shell
pnpm install where-are
```

## Use

To get a file that is in a directory structure as follows:

```
/Users/
    ∟em/
        ∟one/
            ⊢one.txt
            ⊢common.txt
            ∟two/
                ⊢two.txt
                ⊢common.txt
                ∟three/
                    ⊢three.txt
                    ⊢common.txt
```

and our cwd is `/Users/em/one/two/three`:

```typescript
import { whereAre } from 'where-are'

// will find the nearest file's path starting from cwd
whereAre('two.txt') 
// => /Users/em/one/two

// give a relative or absolute path to starting from
whereAre('one.txt', '/Users/em/one/two') 
// => /Users/em/one

// use a predicate to determine whether we should accept the path 
// .. as cwd shows use of related paths and is relative to cwd
whereAre('common.txt', '..', (path: string)=>!path.includes('/two'))
// => /Users/em/one
// ignore cwd and pass predicate as cwd
whereAre('common.txt', (path: string)=>!path.includes('/three'))
// => /Users/em/one/two

// include the filename in the path (starts at cwd) useful when using multiple targets
// multiple targets here means: find 'never-find.txt', 'one.txt' or 'two.txt'
whereAre(['never-find.txt', 'one.txt', 'two.txt'], undefined, undefined, true)
// => /Users/em/one/two/two.txt

// shorthand for the above 
whereAre(['never-find.txt', 'one.txt', 'two.txt'], true)
// => /Users/em/one/two/two.txt
// or with a "defined" predicate
whereAre(['never-find.txt', 'one.txt', 'two.txt'], undefined, true)
// => /Users/em/one/two/two.txt
```
For more examples see tests

## Author

Emmanuel Mahuni

## License

ISC License
