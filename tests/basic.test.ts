import { it, expect, describe } from 'vitest';

import where from '../src';
import { resolve } from 'path';

let cwd: string;

describe(`# where are"`, () => {
  beforeAll(() => {
    cwd = resolve(__dirname, 'fixtures/one/two/three');
    const _cwdFn = process.cwd;
    process.cwd = () => cwd; // we can't use chdir in workers yet (vitest limitation)
    
    return () => {
      process.cwd = _cwdFn;
    };
  });
  
  it(`stops at root dir to avoid infinite loop`, function () {
    const result = where('will-never-find', cwd);
    expect(result).to.be.false;
  });
  
  it.each([
    ['three.txt', 'fixtures/one/two/three'],
    ['two.txt', 'fixtures/one/two'],
    ['two-subdir', 'fixtures/one/two'],
    ['one.txt', 'fixtures/one'],
  ])(`finds target: %o path starting from default cwd: ${cwd}`, (file: string, path: string) => {
    const result = where(file, cwd);
    expect(result).to.be.equal(resolve(__dirname, path));
  });
  
  it.each([
    ['fixtures/one/two/three'],
    ['fixtures/one/two'],
    ['fixtures/one'],
  ])('finds file: one.txt path starting from given cwd: %o', (path: string) => {
    const result = where(`one.txt`, resolve(__dirname, path));
    expect(result).to.be.equal(resolve(__dirname, 'fixtures/one'));
  });
  
  
  it.each([
    [['one.txt', 'never-find.txt'], 'fixtures/one/two/three'],
    [['three.txt', 'one.txt'], 'fixtures/one/two'],
    [['never-find.txt', 'one.txt', 'never-ever-find.txt'], 'fixtures/one'],
  ])(`finds file: one.txt using multiple targets: %o starting from given cwd: %o`, (targets: string[], path: string) => {
    const result = where(targets, resolve(__dirname, path));
    expect(result).to.be.equal(resolve(__dirname, 'fixtures/one'));
  });
  
  
  it.each([
    ['fixtures/one/two/three'],
    ['fixtures/one/two'],
    ['fixtures/one'],
  ])('finds file: "common.txt" path in given cwd: %o (since it is in each directory)', (path: string) => {
    const result = where(`common.txt`, resolve(__dirname, path));
    expect(result).to.be.equal(resolve(__dirname, path));
  });
  
  it.each([
    ['fixtures/one/two/three'],
    ['fixtures/one/two'],
  ])('uses predicate to accept file: "common.txt" path only in "../two/" starting from given cwd: %s', (path: string) => {
    const result = where(`common.txt`, resolve(__dirname, path), (p: string) => !p.includes('three'));
    expect(result).to.be.equal(resolve(__dirname, 'fixtures/one/two'));
  });
  
  it('uses cwd as predicate if defined in cwd parameter', () => {
    const result = where(`common.txt`, (p: string) => !p.includes('three'));
    expect(result).to.be.equal(resolve(__dirname, 'fixtures/one/two'));
  });
  
  describe(`## get filepath instead of just the path there file is in`, function () {
    it('gets path with predicate set as undefined', () => {
      const result = where(`two.txt`, cwd, undefined, true);
      expect(result).to.be.equal(resolve(__dirname, 'fixtures/one/two/two.txt'));
    });
    
    it('gets path with predicate set as the getpath boolean', () => {
      const result = where(`two.txt`, cwd, true);
      expect(result).to.be.equal(resolve(__dirname, 'fixtures/one/two/two.txt'));
    });
    
    it('gets path with cwd set as the getpath boolean', () => {
      const result = where(`two.txt`, true);
      expect(result).to.be.equal(resolve(__dirname, 'fixtures/one/two/two.txt'));
    });
  });
});
