import { existsSync } from 'fs';
import { resolve } from 'path';

/**
 * Find where any of the requested files are starting from given directory or cwd. Also satisfy predicate if given.
 * @param targets
 * @param cwd
 * @param predicate
 * @param getFilePath
 * @returns
 */
export default function whereAre (targets: string[] | string, cwd: string | boolean | Function = process.cwd(), predicate?: Function | boolean, getFilePath?: boolean): string | boolean {
  if (!Array.isArray(targets)) targets = [targets];
  
  if (typeof cwd === 'function') {
    predicate = cwd;
    cwd = process.cwd();
  }
  
  if (typeof predicate === 'boolean') {
    getFilePath = predicate;
    predicate = undefined;
  }
  
  if (typeof cwd === 'boolean') {
    getFilePath = cwd;
    cwd = process.cwd();
  }
  
  let path;
  do {
    for (const name of targets) {
      const cwd_name = resolve(cwd, name);
      if (existsSync(cwd_name)) {
        if (typeof predicate === 'function' && !predicate(cwd)) continue;
        path = getFilePath ? cwd_name : cwd;
        break;
      }
    }
    
    if (!!path || /^\/$|:\\$/.test(cwd)) {
      break; // break if we have a hit or this is the root directory
    }
    
    cwd = resolve(cwd, '..'); // step 1 dir back
  } while (!path);
  
  return !!path && path;
}
